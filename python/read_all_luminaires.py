import socket
from multiprocessing.dummy import Pool


def read_intensity_dali6(ip):
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(timeout)

    # read intensity value
    try:
        resp = send_bytes(sock, ip, 0x01, 0xA0)
        if is_response(resp, 'DALI_CMD OK'):
            int_level = int(resp[12])
            int_perc = round(int_level/254*100)
        else:
            int_level = 'unknown'
            int_perc = 'unknown'
    except socket.timeout:
        int_level = 'timeout'
        int_perc = 'timeout'

    # read cct
    try:
        resp = send_bytes(sock, ip, 0x03, 0xA0)
        if is_response(resp, 'DALI_CMD OK'):
            cct_level = int(resp[12])
            cct_K = round(2800+(7300-2800)*(254-cct_level)/254)
        else:
            cct_level = 'unknown'
            cct_K = 'unknown'
    except socket.timeout:
        cct_level = 'timeout'
        cct_K = 'timeout'

    sock.close()
    return 'Panel at IP {} is set to {} (={}%) intensity at a CCT value of {} (={} K)'.format(ip, int_level, int_perc, cct_level, cct_K)


def read_intensity_dali8(ip):
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(timeout)

    # read intensity value
    try:
        resp = send_bytes(sock, ip, 0x01, 0xA0)
        if is_response(resp, 'DALI_CMD OK'):
            int_level = int(resp[12])
            int_perc = round(int_level/254*100)
        else:
            int_level = 'unknown'
            int_perc = 'unknown'
    except socket.timeout:
        int_level = 'timeout'
        int_perc = 'timeout'
    except:
        print('Unknown error')

    sock.close()
    return 'Spotlight at IP {} is set to {} (={}%) intensity.'.format(ip, int_level, int_perc)


def send_bytes(sock, ip, b1, b2):
    port = 40056
    command = bytearray("DALI_CMD   ", "utf-8")
    command[8] = b1
    command[9] = b2
    # print("Sending byte command " + str(bytes(command)) + ", with inputs " + str(b1) + " and " + str(b2))
    sock.sendto(command, (ip, port))
    resp, server = sock.recvfrom(1024)
    # print('Response from {} to command  {!r}: {!r}'.format(ip, bytes(command), resp))
    return resp


def is_response(response, target):
    resp_str = response[:len(target)].decode("utf-8")
    return resp_str == target


# main script
# IPs: All spotlights and panels in room 1 & 2
spotlight_ips = ('192.168.52.62', '192.168.52.15', '192.168.52.78',
                 '192.168.52.38', '192.168.52.79', '192.168.52.29',
                 '192.168.52.40', '192.168.52.81', '192.168.52.90',
                 '192.168.52.84', '192.168.52.52', '192.168.52.80',
                 '192.168.52.53', '192.168.52.44', '192.168.52.35')
panel_ips = ('192.168.52.39', '192.168.52.37', '192.168.52.32',
             '192.168.52.83', '192.168.52.30', '192.168.52.10',
             '192.168.52.45', '192.168.52.33', '192.168.52.94',
             '192.168.52.82', '192.168.52.42', '192.168.52.55',
             '192.168.52.48', '192.168.52.47', '192.168.52.26',
             '192.168.52.68', '192.168.52.20', '192.168.52.71',
             '192.168.52.28', '192.168.52.43', '192.168.52.73')

# parameters
timeout = 5           # timeout in seconds
parallel = True

if parallel:
    pool = Pool(len(panel_ips) + len(spotlight_ips))
    futures = []

    for panel_ip in panel_ips:
        futures.append(pool.apply_async(read_intensity_dali6, [panel_ip]))

    for spotlight_ip in spotlight_ips:
        futures.append(pool.apply_async(read_intensity_dali8, [spotlight_ip]))

    for i in range(len(futures)):
        resp = futures[i].get() # For each future, wait until the request is finished
        print(resp)

    pool.close()
else:

    for panel_ip in panel_ips:
        print(read_intensity_dali6(panel_ip))

    for spotlight_ip in spotlight_ips:
        print(read_intensity_dali8(spotlight_ip))
