# Test 2: Flash all spotlights 3 times each in intensity, then colour
import socket
from multiprocessing.dummy import Pool


def set_colours(sock, ips, addr, cct, repeats):
    pool = Pool(len(ips))
    futures = []
    for ip in ips:
        futures.append(pool.apply_async(set_colour, [sock, ip, addr, cct, repeats]))
    for future in futures:
        print(future.get()) # For each future, wait until the request is finished and then print the response object.
    pool.close()


def set_colour(sock, ip, addr, cct, repeats):
    success = False
    tries = 1
    while not success and tries <= repeats:
        try:
            value_to_apply = cct
            send_bytes(sock, ip, 0xA3, value_to_apply & 0xFF)  # LSB colour
            send_bytes(sock, ip, 0xC3, ((value_to_apply >> 8) & 0xFF))  # MSB colour
            send_bytes(sock, ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(sock, ip, 2*addr+1, 0xE7)    # Set temporary color temperature
            send_bytes(sock, ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(sock, ip, 2*addr+1, 0xE2)    # Activate
            success = True
        except socket.timeout:
            print('timeout')
            tries += 1

    if not success:
        print("no success after all attempts exhausted")
        raise socket.timeout


def send_bytes(sock, ip, b1, b2):
    port = 40056
    command = bytearray("DALI_CMD   ", "utf-8")
    command[8] = b1
    command[9] = b2
    # print("Sending byte command " + str(bytes(command)) + ", with inputs " + str(b1) + " and " + str(b2))
    sock.sendto(command, (ip, port))
    resp, server = sock.recvfrom(1024)
    # print('Response from {} to command  {!r}: {!r}'.format(ip, bytes(command), resp))
    return resp


# parameters
timeout = 0.3  # timeout in seconds
repeats = 30   # number of times to repeat each command in case of timeout
# ips = ("192.168.52.40", "192.168.52.52")
ips = ("192.168.52.40",
       "192.168.52.81",
       "192.168.52.90",
       "192.168.52.84",
       "192.168.52.52",
       "192.168.52.80",
       "192.168.52.53",
       "192.168.52.44",
       "192.168.52.35")
addr = 0

# Create socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set fade time
sock.settimeout(5)
for ip in ips:
    send_bytes(sock, ip, 0xA3, 0x00)      # set DTR to 0
    send_bytes(sock, ip, 2*addr+1, 0x2E)  # save DTR as fade time for addr 0 (use 2F for fade rate)
    send_bytes(sock, ip, 0x00, 0xfe)  # save DTR as fade time for addr 0 (use 2F for fade rate)
print('All fade times set')

# Change cct back and forth a few times
sock.settimeout(timeout)

# set_colours(sock, ips, addr, 400, repeats)
for i in range(10):
    set_colours(sock, ips, addr, 555, repeats)
    set_colours(sock, ips, addr, 155, repeats)

sock.close()




