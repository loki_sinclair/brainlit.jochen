import socket
from multiprocessing.dummy import Pool


def set_dali6(ip, int_level, cct_level):
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(timeout)
    send_bytes(sock, ip, 0x00, int_level)
    resp = send_bytes(sock, ip, 0x02, cct_level)
    sock.close()
    return resp


def set_dali8(ip, int_level, cct_level):
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(timeout)
    resp = send_bytes(sock, ip, 0x00, int_level)
    set_colour(sock, ip, 0, cct_level, 10)
    sock.close()
    return resp


def set_colour(sock, ip, addr, cct, repeats):
    success = False
    tries = 1
    while not success and tries <= repeats:
        try:
            value_to_apply = cct
            send_bytes(sock, ip, 0xA3, value_to_apply & 0xFF)  # LSB colour
            send_bytes(sock, ip, 0xC3, ((value_to_apply >> 8) & 0xFF))  # MSB colour
            send_bytes(sock, ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(sock, ip, 2*addr+1, 0xE7)    # Set temporary color temperature
            send_bytes(sock, ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(sock, ip, 2*addr+1, 0xE2)    # Activate
            success = True
        except socket.timeout:
            # print('timeout')
            tries += 1

    return success


def send_bytes(sock, ip, b1, b2):
    port = 40056
    command = bytearray("DALI_CMD   ", "utf-8")
    command[8] = b1
    command[9] = b2
    # print("Sending byte command " + str(bytes(command)) + ", with inputs " + str(b1) + " and " + str(b2))
    sock.sendto(command, (ip, port))
    resp, server = sock.recvfrom(1024)
    # print('Response from {} to command  {!r}: {!r}'.format(ip, bytes(command), resp))
    return resp


def is_response(response, target):
    resp_str = response[:len(target)].decode("utf-8")
    return resp_str == target


# main script
spotlight_ips = ('192.168.52.62', '192.168.52.15', '192.168.52.78',
                 '192.168.52.38', '192.168.52.79', '192.168.52.29',
                 '192.168.52.40', '192.168.52.81', '192.168.52.90',
                 '192.168.52.84', '192.168.52.52', '192.168.52.80',
                 '192.168.52.53', '192.168.52.44', '192.168.52.35')
panel_ips = ('192.168.52.39', '192.168.52.37', '192.168.52.32',
             '192.168.52.83', '192.168.52.30', '192.168.52.10',
             '192.168.52.45', '192.168.52.33', '192.168.52.94',
             '192.168.52.82', '192.168.52.42', '192.168.52.55',
             '192.168.52.48', '192.168.52.47', '192.168.52.26',
             '192.168.52.68', '192.168.52.20', '192.168.52.71',
             '192.168.52.28', '192.168.52.43', '192.168.52.73')

# parameters
timeout = 5           # timeout in seconds
panel_int = 58        # desired panel intensity in percent
panel_cct = 6800      # desired panel cct in K
spotl_int = 60        # desired spotlight cct in percent
spotl_cct = 2000      # desired spotlight cct in K
fadetime  = 0


# # Set fade time on all luminaires
# sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# sock.settimeout(5)
# for panel_ip in panel_ips:
#     send_bytes(sock, panel_ip, 0xA3, fadetime)  # set DTR to fadetime
#     send_bytes(sock, panel_ip, 0x01, 0x2E)      # save DTR as fade time for addr 0 (use 2F for fade rate)
#     send_bytes(sock, panel_ip, 0xA3, fadetime)  # set DTR to fadetime
#     send_bytes(sock, panel_ip, 0x03, 0x2E)      # save DTR as fade time for addr 0 (use 2F for fade rate)
# for spotlight_ip in spotlight_ips:
#     send_bytes(sock, spotlight_ip, 0xA3, fadetime)  # set DTR to fadetime
#     send_bytes(sock, spotlight_ip, 0x01, 0x2E)      # save DTR as fade time for addr 0 (use 2F for fade rate)
# sock.close()

pool = Pool(len(panel_ips) + len(spotlight_ips))
futures = []

for panel_ip in panel_ips:
    int_level = round(254/100*panel_int)
    cct_level = round((7300-panel_cct)/(7300-2800)*254)
    futures.append(pool.apply_async(set_dali6, [panel_ip, int_level, cct_level]))
    print('Panel at IP {} was set to {} (={}%) intensity at a CCT value of {} (={} K)'.format(panel_ip, int_level, panel_int, cct_level, panel_cct))

for spotlight_ip in spotlight_ips:
    int_level = round(254/100*spotl_int)
    cct_level = round((6500-spotl_cct)/(6500-1800)*254)
    futures.append(pool.apply_async(set_dali8, [spotlight_ip, int_level, cct_level]))
    print('Spotlight at IP {} was set to {} (={}%) intensity at a CCT value of {} (={} K)'.format(spotlight_ip, int_level, spotl_int, cct_level, spotl_cct))

for i in range(len(futures)):
    resp = futures[i].get() # For each future, wait until the request is finished

pool.close()