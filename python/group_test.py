# This is a test to check how closely luminaires can be synchronised and how reliable they are when connected to a single node

import time
import socket
import logging


def send_bytes(ip, b1, b2):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    command = bytearray("DALI_CMD   ", "utf-8")
    command[8] = b1
    command[9] = b2
    sock.sendto(command, (ip, 40056))
    resp, server = sock.recvfrom(1024)
    sock.close()
    return resp


def reset(ip):
    # Commissioning routine for a node
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(20)
    resp = send_command_byte(sock, ip, bytes("DALI_ERASE", "utf-8"))
    if is_response(resp, "DALI_ERASE OK") or is_response(resp, "DALI_ERASE KO"):
        logging.info("Erased successfully")
        resp = send_command_byte(sock, ip, bytes("DALI_INIT_COMM", "utf-8"))
        if is_response(resp, "DALI_INIT_COMM OK"):
            logging.info("Commissioning started successfully")
            resp = send_command_byte(sock, ip, bytes("DALI_ST_COMM", "utf-8"))
            print(chr(resp[8]))
            while is_response(resp, "RUNNING"):
                logging.info("Commissioning running, found %d outputs so far", int(chr(resp[8])))
                time.sleep(1)
                resp = send_command_byte(sock, ip, bytes("DALI_ST_COMM", "utf-8"))
            if is_response(resp, "FINISHED"):
                logging.info("Commissioning finished, found %d outputs", int(chr(resp[9])))
            else:
                logging.error("Unexpected response: %s", resp)
        else:
            logging.error("Unexpected response: %s", resp)
    else:
        logging.error("Unexpected response: %s", resp)
    sock.close()


def send_command_byte(sock, ip, command):
    sock.sendto(command, (ip, 40056))
    resp, server = sock.recvfrom(1024)
    logging.debug('Response from {} to command  {!r}: {!r}'.format(ip, bytes(command), resp))
    return resp


def is_response(response, target):
    resp_str = response[:len(target)].decode("utf-8")
    return resp_str == target


# main script
ip = "192.168.52.83"
num_lums = 1  # how many luminaires are connected to the ip?

logging.basicConfig(level=logging.DEBUG)

# STEP 1:
# When the luminaires are newly connected to the node, run this command.
# It will take 1 minute or more to run and should end with a number of devices
# found that is TWICE the number of luminaires connected to the node.

# uv.reset(ip)

# STEP 2:
# Add all intensities to group 0 and all colours to group 1
for i in range(num_lums):
    int_addr = i*4 + 1  # address for special intensity commands
    cct_addr = i*4 + 3  # address for special colour commands
    send_bytes(ip, int_addr, 96)  # add to group 0
    send_bytes(ip, cct_addr, 97)  # add to group 1

# STEP 3:
# Set fade times
send_bytes(ip, 0xA3, 5)  # set DTR to fadetime
send_bytes(ip, 129, 0x2E)  # set group 0 to that fadetime
send_bytes(ip, 0xA3, 5)  # set DTR to fadetime
send_bytes(ip, 131, 0x2E)  # set group 1 to that fadetime

# STEP 4:
# Run a few colour changes to see the effects of grouping

send_bytes(ip, 129, 0x00)  # switch off intensity fast
send_bytes(ip, 131, 0x00)  # switch colour to cold white fast

# slowly ramp on
time.sleep(1)
send_bytes(ip, 128, 0xFE)
send_bytes(ip, 130, 0xFE)
time.sleep(3)
#
for i in range(5):
    send_bytes(ip, 128, 0xFE)
    send_bytes(ip, 130, 0x00)
    time.sleep(3)
    send_bytes(ip, 128, 0x00)
    time.sleep(3)

    send_bytes(ip, 128, 0xFE)
    send_bytes(ip, 130, 0xFE)
    time.sleep(3)
    send_bytes(ip, 128, 0x00)
    time.sleep(3)


