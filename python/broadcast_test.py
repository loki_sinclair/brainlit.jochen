# import logging
import requests
import time
import socket


def set_lum_list(token, lumids, targetints):
    datastring = ""
    for lumid in range(len(lumids)):
        datastring = datastring + "id=" + str(lumids[lumid]) + "&dimming=" \
                     + str(targetints[lumid]) + ";"
    datastring = datastring[:-1]  # remove final ";"
    print("\t\tCall" + str(datastring))
    temp = requests.post("http://192.168.52.210/API/web/index.php/"
                         + "commands/overridegroup",
                         data=datastring, auth=("delay_test", token))
    print("\t\tResponse" + str(temp.json()))
    return

def set_colour(ip, addr, cct, repeats=1):
    success = False
    tries = 1
    while not success and tries <= repeats:
        try:
            value_to_apply = cct
            send_bytes(ip, 0xA3, value_to_apply & 0xFF)  # LSB colour
            send_bytes(ip, 0xC3, ((value_to_apply >> 8) & 0xFF))  # MSB colour
            send_bytes(ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(ip, 2*addr+1, 0xE7)    # Set temporary color temperature
            send_bytes(ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(ip, 2*addr+1, 0xE2)    # Activate
            success = True
        except socket.timeout:
            print('timeout')
            tries += 1

    if not success:
        print("no success after all attempts exhausted")
        raise socket.timeout

def send_bytes(ip, b1, b2):
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(5)
    port = 40056
    command = bytearray("DALI_CMD   ", "utf-8")
    command[8] = b1
    command[9] = b2
    # print("Sending byte command " + str(bytes(command)) + ", with inputs " + str(b1) + " and " + str(b2))
    sock.sendto(command, (ip, port))
    resp, server = sock.recvfrom(1024)
    # print('Response from {} to command  {!r}: {!r}'.format(ip, bytes(command), resp))
    sock.close()
    return resp

# parameters
ip = "192.168.52.255"  # broadcast IP
sleeptime = 5          # time to sleep between tests
single_ip = "192.168.52.10"

# Test 1: Broadcast intensities
# Switch everything off, then on again
print('Broadcasting intensity 0%')
# send_bytes(ip, 0x00, 0x00)
# time.sleep(1)
print('Broadcasting intensity 50%')
# send_bytes(ip, 0x00, 0x79)
# time.sleep(sleeptime)


# Test 2: Broadcast dali6 colours
# Switch everything off, then on again
print('Broadcasting panel CCT 7300')
# send_bytes(ip, 0x02, 0x00)
# time.sleep(1)
print('Broadcasting panel CCT 2800')
# send_bytes(ip, 0x02, 0xFE)
# time.sleep(sleeptime)


# Test 3: Broadcast dali8 colours
# Switch everything off, then on again
print('Broadcasting spot CCT 6500')
# set_colour(ip, 0, 155)
# time.sleep(1)
print('Broadcasting spot CCT 1800')
set_colour(ip, 0, 555)
# time.sleep(sleeptime)


# Test 4: Broadcast fade time
# Sets fade time to 0, queries it on a single IP, then switches that IP off, then
# Sets fade time to 5, queries it on a single IP, then switches that IP on
send_bytes(single_ip, 0xA3, 0x00)          # set DTR to 0 (<0.7s) or 5 (2.8s) or 9 (11.3s)
print('Broadcasting fade time of 0')
send_bytes(single_ip, 0x01, 0x2E)          # save DTR as fade time for addr 0 (use 2F for fade rate)
resp = send_bytes(single_ip, 0x01, 0xA5)   # query fade time/fade rate on one luminaire
ft = resp[12]
print('IP {} has a fade time of {} (and a fade rate of {})'.format(single_ip, ft >> 4, ft & 0x0F))
# send_bytes(single_ip, 0x00, 0x00)   # switch off
time.sleep(0.5)
send_bytes(ip, 0xA3, 0x05)          # set DTR to 0 (<0.7s) or 5 (2.8s) or 9 (11.3s)
print('Broadcasting fade time of 5')
send_bytes(ip, 0x01, 0x2E)          # save DTR as fade time for addr 0 (use 2F for fade rate)
resp = send_bytes(single_ip, 0x01, 0xA5)   # query fade time/fade rate on one luminaire
ft = resp[12]
print('IP {} has a fade time of {} (and a fade rate of {})'.format(single_ip, ft >> 4, ft & 0x0F))
# send_bytes(single_ip, 0x00, 0xAA)   # switch off


