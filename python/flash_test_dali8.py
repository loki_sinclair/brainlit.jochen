# Test 1: Flash a single spotlight 3 times each in intensity, then colour
import socket

def set_colour(sock, ip, addr, cct, repeats):
    success = False
    tries = 1
    while not success and tries <= repeats:
        try:
            value_to_apply = cct
            send_bytes(sock, ip, 0xA3, value_to_apply & 0xFF)  # LSB colour
            send_bytes(sock, ip, 0xC3, ((value_to_apply >> 8) & 0xFF))  # MSB colour
            send_bytes(sock, ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(sock, ip, 2*addr+1, 0xE7)    # Set temporary color temperature
            send_bytes(sock, ip, 0xC1, 0x08)        # Enable device DALI8 type
            send_bytes(sock, ip, 2*addr+1, 0xE2)    # Activate
            success = True
        except socket.timeout:
            print('timeout')
            tries += 1

    if not success:
        print("no success after all attempts exhausted")
        raise socket.timeout

def send_bytes(sock, ip, b1, b2):
    port = 40056
    command = bytearray("DALI_CMD   ", "utf-8")
    command[8] = b1
    command[9] = b2
    # print("Sending byte command " + str(bytes(command)) + ", with inputs " + str(b1) + " and " + str(b2))
    sock.sendto(command, (ip, port))
    resp, server = sock.recvfrom(1024)
    # print('Response from {} to command  {!r}: {!r}'.format(ip, bytes(command), resp))
    return resp

# main script
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # create socket

# parameters
timeout = 0.3           # timeout in seconds
repeats = 30            # number of times to repeat each command in case of timeout
ip = "192.168.52.40"    # spotlight IP (40 is in the back left corner of room 1)
addr = 0

#set fade time
sock.settimeout(5)
send_bytes(sock, ip, 0xA3, 0x00)      # set DTR to 0
send_bytes(sock, ip, 2*addr+1, 0x2E)  # save DTR as fade time for addr 0 (use 2F for fade rate)

# Change cct back and forth a few times
sock.settimeout(timeout)

for i in range(3):
    send_bytes(sock, ip, 2*addr, 0x00)  # 0%
    send_bytes(sock, ip, 2*addr, 0xfe)  # 100%

for j in range(3):
    set_colour(sock, ip, addr, 555, repeats)
    set_colour(sock, ip, addr, 155, repeats)

sock.close()




